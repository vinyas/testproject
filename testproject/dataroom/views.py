from django.shortcuts import render
from .models import Folder
# Create your views here.


def get_folders(request):
    return render(request, "folders.html", {'folders': Folder.objects.all()})
