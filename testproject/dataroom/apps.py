from django.apps import AppConfig


class DataroomConfig(AppConfig):
    name = 'testproject.dataroom'
