from django.urls import path

from .views import get_folders

app_name = "users"
urlpatterns = [
    path("", view=get_folders, name="get_folders"),

]
